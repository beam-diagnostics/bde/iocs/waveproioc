< envVars

errlogInit(20000)

dbLoadDatabase("$(TOP_DIR)/dbd/$(APP).dbd")
$(APP)_registerRecordDeviceDriver(pdbbase)

# specify the TCP endpoint and port name
epicsEnvSet("LOCATION",                     "LAB")
epicsEnvSet("DEVICE_IP",                    "bd-scp02.cslab.esss.lu.se")
epicsEnvSet("DEVICE_NAME",                  "SCP-02")
epicsEnvSet("PREFIX",                       "$(LOCATION):$(DEVICE_NAME):")
epicsEnvSet("PORT",                         "WAVEPRO")

# configure StreamDevice path
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(DB_DIR)")

# report streamdevice errors
var streamError 1
# report streamdevice debug messages (LOTS!)
#var streamDebug 1

vxi11Configure("$(PORT)", "$(DEVICE_IP)", 0, "0.0", "inst0", 0)

# laod database defining the EPICS records
dbLoadRecords(wavepro.db, "P=$(PREFIX),R=,PORT=$(PORT)")

# asynSetTraceIOMask("$(PORT)",0,2)
# asynSetTraceMask("$(PORT)",0,255)

###############################################################################
iocInit
###############################################################################

date
###############################################################################
